<?php

namespace NalTest\Common;

class Util
{
    // view of header
    protected $viewHeader;
    // view of footer
    protected $viewFooter;
    // view of footer
    /** 
     * render view
     * 
     * @param string file view name 
    */
    public function loadView($viewName, $data = [])
    {
        $this->loadFile($this->viewHeader);
        $this->loadFile($viewName, $data);
        $this->loadFile($this->viewFooter);
    }
    /**
     * set view of header
     * 
     * @param string file view name 
     */
    public function setHeader($viewName)
    {
        $this->viewHeader = $viewName;
    }
    /**
     * set view of footer
     * 
     * @param string file view name 
     */
    public function setFooter($viewName)
    {
        $this->viewFooter = $viewName;
    }
    /** 
    * redirect to url
    *
    */
    public function redirect($url)
    {
        $uri = BASE_URL . $url;
        header('Location: ' . $uri, true, 301);
        exit();
    }
    /**
     * load file
     * 
     * @param string file name
     */
    private function loadFile($filename, $data = [])
    {
        extract($data);

        $patchFile = FULL_PATCH . '/app/views/' . $filename . '.php';
        // check file exits;
        if (is_file($patchFile)) {
            require $patchFile;
        } else {
            exit("The {$patchFile} file doesn't exist");
        }
    }
}