<?php

namespace NalTest\App\Controller;

use NalTest\Common\Util;

class BaseController
{
    protected $util;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION)) {
            @session_start();
        }
        $this->util = new Util;
        // load headers
        $this->util->setHeader('header');
        // load footer
        $this->util->setFooter('footer');
    }
}