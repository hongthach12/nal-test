<?php

namespace NalTest\App\Controller;

use NalTest\Model\TodoList;

class TodoController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
    *   index
    */
    public function index()
    {
        $objTodoList = new TodoList;
        $list = [];
        $todo = [];
        $errors = null;
        if(isset($_GET['todo']) && $_GET['todo']['priod'] != '') {
            $todo = $_GET['todo'];
            $list = $objTodoList->getTodoListByPeriod($todo['priod'], $todo['searchType']);    
        } else {
            $list = $objTodoList->getTodoList();
        }
        $listSearchType = $objTodoList->getSearchType();

        $this->util->loadView('todo-listing', ['list' => $list, 'listSearchType' => $listSearchType, 'todo' => $todo]);
    }
    /**
    *   add new todo
    */
    public function add()
    {
        $objTodoList = new TodoList();
        $list = $objTodoList->getTodoList();
        $result = null;

        $listStatus = $objTodoList->getListStatus();

        if(!empty($_POST['todo'])) {
            $todo = $_POST['todo'];
            // add todo
            $result = $objTodoList->addTodo($todo);
            // if correct then redirect to homepage
            if($result){
                return $this->util->redirect('/');
            }
        }
        return $this->util->loadView('add-todo', ['result' => $result, 'listStatus' => $listStatus]);
    }
    public function edit($idx)
    {
        $objTodoList = new TodoList();
        $todo = $objTodoList->getTodo($idx);
        $result = null;
        if(!empty($_POST['todo'])) {
            $todoRequest = $_POST['todo'];
            // add todo
            $result = $objTodoList->editTodo($idx, $todoRequest);
            // if update success then re-get it
            if($result) {
                $todo = $objTodoList->getTodo($idx);
            }
        }
        $listStatus = $objTodoList->getListStatus();        

        return $this->util->loadView('edit-todo', [
                'todo' => $todo, 
                'result' => $result,
                'listStatus' => $listStatus,
                'idx' => $idx
            ]);
    }
    /**
    *   delete todo
    *   @param interge index of todo
    */
    public function delete($idx)
    {
        $objTodoList = new TodoList();
        $objTodoList->deleteTodo($idx);

        return $this->util->redirect('/');
    }
    /**
    *   delele all todo
    */
    public function emptyAll()
    {
        $objTodoList = new TodoList();
        $objTodoList->deleteAll();

        $this->util->redirect('/');
    }
}