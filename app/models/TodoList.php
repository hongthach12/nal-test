<?php 

namespace NalTest\Model;

use DateTime;
use InvalidArgumentException;

class TodoList {

	const STATUS_PLANNING = 'Planning';
	const STATUS_DOING = 'Doing';
	const STATUS_COMPLETE = 'Complete';

    const TYPE_MONTH = 'm';
    const TYPE_WEEK = 'W';
    const TYPE_DATE = 'Y-m-d';
	 
	protected $todoList = [];
    protected $format = 'Y-m-d';
    protected $attrs = [
        'start_date',
        'end_date',
        'work_name',
        'status'
    ];

    public function __construct()
    {
        if(!isset($_SESSION['todoList'])){
            $_SESSION['todoList'] = [];
        }
    }
    /**
     * add todo to list
     * @param array $todo
     * @return integer index of todo
     */
	public function addTodo($todo)
	{
        if($this->isValidTodo($todo)) {
            // $this->todoList[] = $todo;  
            $list = $_SESSION['todoList'];
            $list[] = $todo;
            // set todolist into session
            $_SESSION['todoList'] = $list;
            return true;
        } else {
            return false;
        }
	}
	/**
	 * edit a todo
	 * @param  integer $idx index of list todo
	 * @param  array $todo
	 * @return boolean
	 */
	public function editTodo($idx, $todo)
	{
        $list = $_SESSION['todoList'];
		if(!empty($list[$idx]) && $this->isValidTodo($todo)) {
			$list[$idx] = $todo;
            // set todolist into session
            $_SESSION['todoList'] = $list;

			return true;
		} else {
			return false;
		}
	}
    /**
     * delete todo
     * @param  [type] $idx index of list todo
     * @return [boolean]
     */
	public function deleteTodo($idx)
	{
        $list = $_SESSION['todoList'];

        if(!empty($list[$idx])) {
            unset($list[$idx]);
            // set todolist into session
            $_SESSION['todoList'] = $list;
            return true;
        } else {
            return false;
        }
	}
    /**
     * get todo list by week
     * @param  string  week or month or date
     * @param  integer $type
     * @return array todo list
     */
	public function getTodoListByPeriod($period, $type)
	{
        $result = [];
        $list = $_SESSION['todoList'];
        // if invalid date then return empty
        if($type == self::TYPE_DATE && !$this->validDate($period)) {
            return $result;
        }
        foreach ($list as $key => $todo) {
            $dateStart = new DateTime($todo['start_date']);
            $dateEnd = new DateTime($todo['end_date']);
            if($type == self::TYPE_DATE) {
                $selectDate = new DateTime($period);
                if($dateStart <= $selectDate && $selectDate <= $dateEnd){
                    $result[] = $todo;
                }
            } else {
                if($dateStart->format($type) <= $period && $period <= $dateEnd->format($type)) {
                    $result[] = $todo;
                }
            }
        }
        return $result;
	}
	/**
	 * check valid date and start date must be before end date
	 * @param  Date $startDate
	 * @param  Date $endDate
	 * @return [boolean]
	 */
	public function checkDateRange($startDate, $endDate)
	{
        if(!$this->validDate($startDate) || !$this->validDate($endDate)) {
            return false;
        } else {
            $dateStart = new DateTime($startDate);
            $dateEnd = new DateTime($endDate);
            return $dateEnd >= $dateStart;
        }
    }
    /** 
     * get todo list
     * 
     * @return array
     */
    public function getTodoList()
    {
        return $_SESSION['todoList'];
    }

    /** 
     * get todo list
     * 
     * @param integer index of todo
     * @return array
     */
    public function getTodo($idx)
    {
        $list = $_SESSION['todoList'];
        
        if(empty($list[$idx])){
            return [];
        }
        return $list[$idx];
    }
    /** 
     * delete all todo
     * 
     */
    public function deleteAll()
    {
        $_SESSION['todoList'] = [];
    }
    public function getSearchType()
    {
        return ['Month' => self::TYPE_MONTH, 'Week' => self::TYPE_WEEK, 'Date' => self::TYPE_DATE];
    }
    public function getListStatus()
    {
        return [self::STATUS_PLANNING, self::STATUS_DOING, self::STATUS_COMPLETE];
    }
    /** 
     * check valid date
     * 
     * @param string date
     * @return boolean
     */
	private function validDate($date)
	{
        $d = DateTime::createFromFormat($this->format, $date);

        return $d && $d->format($this->format) === $date;
	}
    /**
     * check status, start date, end date
     * @param  array  $todo
     * @return boolean
     */
    private function isValidTodo($todo)
    {
        $keys = array_keys($todo);
        // check array have keys was defined 
        if($this->isExitsAttributes($keys)) {
            // check status is valid
            // start data, end date is valid
            return in_array($todo['status'], [self::STATUS_PLANNING, self::STATUS_DOING, self::STATUS_COMPLETE])
                    && $this->validDate($todo['start_date'])
                    && $this->validDate($todo['end_date'])
                    && $this->checkDateRange($todo['start_date'], $todo['end_date']);
        } else {
            return false;
        }
    }
    private function isExitsAttributes($keys)
    {   
        $result = 0;
        foreach($this->attrs as $attr) {
            // exits a key then add 1 more;
            $result += in_array($attr, $keys);
        }
        // return true if result equal length list attr
        return $result === count($this->attrs);
    }
}