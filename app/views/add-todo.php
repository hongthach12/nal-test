
<?php if($result === false) echo '**** paramaters invalid'; ?>
<form method="POST" action="<?php echo BASE_URL ?>/todo/add">
    <table>
        <tr>
            <td>Work Name</td>
            <td><input type="text" name="todo[work_name]"></td>
        </tr>
        <tr>
            <td>Start Date</td>
            <td><input type="text" name="todo[start_date]" placeholder="Y-m-d"></td>
        </tr>
        <tr>
            <td>End Date</td>
            <td><input type="text" name="todo[end_date]" placeholder="Y-m-d"></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
            <select name="todo[status]" id="">
                <?php foreach($listStatus as $status) { ?>
                    <option value="<?= $status ?>"><?= $status ?></option>
                <?php } ?>
            </select>
            </td>
        </tr>
    </table>
    <button type="submit">Add New</button>
</form>
<a href="<?php echo BASE_URL ?>">Back</a>