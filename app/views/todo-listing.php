
<form method="GET" action="<?php echo BASE_URL ?>/todo/index/">
    <input type="text" name="todo[priod]" value="<?= @$todo['priod']?>" placeholder="By Month/ Week/ Date">
    <select name="todo[searchType]" id="">
        <?php foreach($listSearchType as $name => $value) { ?>
            <option <?php echo !empty($todo) && $value == $todo['searchType'] ? 'selected' : '' ?> value="<?= $value ?>"><?= $name ?></option>
        <?php } ?>
    </select>
    <button type="submit">Search</button>
</form>
<br>
<table border="1">
    <tr>
        <td>Work name</td>
        <td>Start date</td>
        <td>End date</td>
        <td>Status</td>
        <td>Action</td>
    </tr>
    <?php
        $url = BASE_URL;
        foreach($list as $key => $item) {
            $tr = "<tr>";
            $tr .= "<td>{$item['work_name']}</td>";
            $tr .= "<td>{$item['start_date']}</td>";
            $tr .= "<td>{$item['end_date']}</td>";
            $tr .= "<td>{$item['status']}</td>";
            $tr .= "<td><a href='{$url}/todo/edit/{$key}'>Edit | </a>";
            $tr .= "<a href='{$url}/todo/delete/{$key}'>Delete</a></td>";
            $tr .= "</tr>";
            echo $tr;
        }
    ?>
    
</table>
<br>
<a href="<?php echo BASE_URL ?>/todo/add">Add New Todo</a>

<form method="GET" action="<?php echo BASE_URL ?>/todo/emptyAll">
    <button type="submit">Remove ALL</button>
</form>