<?php
	
	namespace NalTest;

	require __DIR__.'/vendor/autoload.php';
	// follow PSR
	ob_start();
	session_start();
	Define('FULL_PATCH', __DIR__ . '/');
	Define('BASE_URL', "http://$_SERVER[HTTP_HOST]");
	
	$url = !empty($_GET['url']) ? $_GET['url'] : null;

	$url = rtrim($url, '/\\');

	$arrUrl = explode('/', $url);
	$controller = !empty($arrUrl[0]) ? $arrUrl[0] : "todo";
	$action = isset($arrUrl[1]) ? $arrUrl[1] : "index";
	$param = isset($arrUrl[2]) ? $arrUrl[2] : null;
    $namespace = '\\NalTest\\App\\Controller\\';
	$fileName = FULL_PATCH . 'app/controllers/' . ucfirst($controller) . 'Controller.php';

	if(file_exists($fileName)) {

		$className = ucfirst($controller) . 'Controller';

		$pCtrl = $namespace . $className;

		$object = new $pCtrl();

		if(!method_exists($object, $action)) {
			echo 'Method not found!';
		} else {
			if(isset($param)) {
				$object->$action($param);
			} else {
				$object->$action();
			}
		}
	} else {
		echo 'Controller not found';
	}