<?php

use PHPUnit\Framework\TestCase;
use NalTest\Model\TodoList;

session_start();

final class TodoListTest extends TestCase
{
    public function testCanBeCreatedValidTodo()
    {
        $todoList = new TodoList();
        $todo = [
            'start_date' => '2018-07-04',
            'end_date' => '2018-07-04',
            'work_name' =>'ABC',
            'status'  => TodoList::STATUS_PLANNING
        ];
        $this->assertTrue($todoList->addTodo($todo));
    }
    public function testInvalidStartDate()
    {
        $todoList = new TodoList();
        $todo = [
            'start_date' => '2018-15-15',
            'end_date' => '2018-08-05',
            'work_name' =>'ABC',
            'status'  => TodoList::STATUS_PLANNING
        ];
        $this->assertFalse($todoList->addTodo($todo));
    }
    public function testInvalidEndDate()
    {
        $todoList = new TodoList();
        $todo = [
            'start_date' => '2018-08-04',
            'end_date' => '2018-08-55',
            'work_name' =>'ABC',
            'status'  => TodoList::STATUS_PLANNING
        ];
        $this->assertFalse($todoList->addTodo($todo));
    }
    public function testAddInvalidStartDateGreatThanEndDateTodo()
    {
        $todoList = new TodoList();
        $todo = [
            'start_date' => '2018-08-06',
            'end_date' => '2018-08-05',
            'work_name' =>'ABC',
            'status'  => TodoList::STATUS_PLANNING
        ];
        $this->assertFalse($todoList->addTodo($todo));
    }

    public function testEditTodo()
    {
        $todoList = new TodoList();
        // get todo by index 0 had inserted before;
        $todo = $todoList->getTodo(0);
        $todoEdit = [
            'start_date' => '2018-08-04',
            'end_date' => '2018-08-05',
            'work_name' =>'JOB 2',
            'status'  => TodoList::STATUS_COMPLETE
        ];
        $todoList->addTodo($todo);

        $todoList->editTodo(0, $todoEdit);

        $todoByIdx = $todoList->getTodo(0);

        $this->assertEquals($todoByIdx, $todoEdit);
    }
    public function testDeleteTodo()
    {
        $todoList = new TodoList();

        $todoList->deleteTodo(0);

        $this->assertEmpty($todoList->getTodo(0));
    }
    public function testGetTodoByWeek()
    {
        $todoList = new TodoList();
        
        $todo1 = [
            'start_date' => '2018-08-04',
            'end_date' => '2018-08-04',
            'work_name' =>'JOB 1',
            'status'  => TodoList::STATUS_PLANNING
        ];
        $todo2 = [
            'start_date' => '2018-08-07',
            'end_date' => '2018-08-07',
            'work_name' =>'JOB 1',
            'status'  => TodoList::STATUS_PLANNING
        ];
        
        $todoList->addTodo($todo1);
        $todoList->addTodo($todo2);

        $this->assertEquals($todoList->getTodoListByPeriod(31, TodoList::TYPE_WEEK), [$todo1]);
    }
    public function testGetTodoByMonth()
    {
        $todoList = new TodoList();
        
        $todo1 = [
            'start_date' => '2018-05-04',
            'end_date' => '2018-05-04',
            'work_name' =>'JOB 1',
            'status'  => TodoList::STATUS_PLANNING
        ];
        
        $todoList->addTodo($todo1);

        $this->assertEquals($todoList->getTodoListByPeriod("05", TodoList::TYPE_MONTH), [$todo1]);
    }

    public function testGetTodoByDay()
    {
        $todoList = new TodoList();
        
        $todo1 = [
            'start_date' => '2018-06-20',
            'end_date' => '2018-06-25',
            'work_name' =>'JOB 1',
            'status'  => TodoList::STATUS_PLANNING
        ];
       
        $todoList->addTodo($todo1);

        $this->assertEquals($todoList->getTodoListByPeriod("2018-06-22", TodoList::TYPE_DATE), [$todo1]);
    }
}

